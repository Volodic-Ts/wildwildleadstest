package com.volodic.wildwildleadstest.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.volodic.wildwildleadstest.R;
import com.volodic.wildwildleadstest.databinding.FragmentSplashBinding;

import java.util.HashMap;
import java.util.Map;

public class SplashFragment extends Fragment {

    private final static String TAG = "SPLASH_FRAGMENT";

    private FragmentSplashBinding binding;

    private Map<String, Object> defaults;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentSplashBinding.inflate(inflater, container, false);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        defaults = new HashMap<>();

        defaults.put("type_of_screen", true);

        FirebaseRemoteConfig mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        mFirebaseRemoteConfig.setDefaultsAsync(defaults);
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(5)
                .build();
        mFirebaseRemoteConfig.setConfigSettingsAsync(configSettings);

        boolean flag = mFirebaseRemoteConfig.getBoolean("type_of_screen");

        mFirebaseRemoteConfig.activate();
        mFirebaseRemoteConfig.fetch();

        if (flag) {
            Navigation.findNavController(requireActivity(), R.id.nav_host_fragment).navigate(R.id.naigation_content);
        } else {
            Navigation.findNavController(requireActivity(), R.id.nav_host_fragment).navigate(R.id.navigation_game);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
